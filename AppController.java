package me.demo;

import java.util.List;
import javax.validation.Valid;
import org.springframework.hateoas.EntityModel;
import org.springframework.hateoas.Link;
import org.springframework.hateoas.server.mvc.WebMvcLinkBuilder;
import static org.springframework.hateoas.server.mvc.WebMvcLinkBuilder.linkTo;
import static org.springframework.hateoas.server.mvc.WebMvcLinkBuilder.methodOn;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping("/app")
public class AppController {

    private static final DAOService ds = new DAOService();

    @GetMapping("/all")
    public Object getAll() {
        List<Application> apps = ds.getAllUsers();        
        if(apps == null)    return "No users found!";
        return apps;        
    }

    @GetMapping("/{id}/app")
    public Object getOne(@PathVariable int id) {
        Application a = ds.getOne(id);
        if(a == null)   return "No user found!";
        EntityModel<Object> res = EntityModel.of(a);
        WebMvcLinkBuilder link = linkTo(methodOn(this.getClass()).getAll());
	res.add(link.withRel("all-users"));
        return res;
    }

    @PostMapping("")
    public Object create(@Valid @RequestBody Application app) {
        Application res = ds.create(app);
        if(res == null)   return "Error!";
        Link link = linkTo(methodOn(this.getClass())
                .getAll())
                .withRel("Applicant added!");
        return link;
    }

    @DeleteMapping("/{id}")
    public Object delete(@PathVariable int id) {
        boolean res = ds.delete(id);
        if(!res)    return "Error!";
        Link link = linkTo(methodOn(this.getClass())
                .getAll())
                .withRel("Applicant deleted!");
        return link;
    }

    @PutMapping("/{id}/app/{mem}")
    public Object update(@PathVariable int id, 
            @Valid @PathVariable int mem, 
            @RequestBody Applicant app) {
        boolean res = ds.update(id, mem, app);
        if(res == false) return "Error!";
        Link link = linkTo(methodOn(this.getClass())
                .getAll())
                .withRel("Applicant updated!");
        return link;
    }
}
