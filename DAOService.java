package me.demo;

import java.util.ArrayList;
import java.util.List;
import java.util.NoSuchElementException;

public class DAOService {
    
    private static final List<Application> apps = new ArrayList<>();
    private static int count = 0;
    
    public List<Application> getAllUsers() {
        if(apps.isEmpty())  return null;
        return apps;
    }

    public Application getOne(int id) {
        try {
            Application u = apps.stream()
                    .filter(x -> x.getId() == id)
                    .findFirst().get();
            return u;
        } catch (NoSuchElementException w) {
            return null;
        }
    }

    public Application create(Application app) {
        int ch = count;
        app.setId(count++);                 // set id
        apps.add(app);
        if(count == ch)  return null;       // number of apps same or not
        return app;
    }
   
    public boolean update(int id, int mem, Applicant app) {
        Application u = getOne(id);
        if(u == null)    return false;
        Applicant uz = u.getApps().stream()
                .filter(x -> x.getMem() == mem)
                .findFirst().get();
        if(uz == null)   return false;      // if member not found
        uz.setMem(app.getMem());
        uz.setName(app.getName());
        return u.getApps()
                .get(id).getName()
                .equals(app.getName());
    }
   
    public boolean delete(int id) {
        Application u = getOne(id);
        if(u == null)    return false;
        apps.remove(u);
        return getOne(id) == null;
    }
}
