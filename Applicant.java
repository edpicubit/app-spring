package me.demo;

import javax.validation.constraints.NotBlank;
import org.hibernate.validator.constraints.Range;

public class Applicant {
    
    @NotBlank                         // not blank and not null
    private String name;

    @Range(min = 1)                   // must have one digit
    private int mem;

    public Applicant(String name, int mem) {
        this.name = name;
        this.mem = mem;
    }
    
    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public int getMem() {
        return mem;
    }

    public void setMem(int mem) {
        this.mem = mem;
    }

    @Override
    public String toString() {
        return "Applicant{" + "name=" + name + ", mem=" + mem + '}';
    }
    
}
