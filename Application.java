package me.demo;

import java.util.List;

public class Application {
    
    private int id;
    private String type;
    private List<Applicant> apps;

    public Application(String type, List<Applicant> apps) {
        this.type = type; this.apps = apps;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }

    public List<Applicant> getApps() {
        return apps;
    }

    public void setApps(List<Applicant> apps) {
        this.apps = apps;
    }

    @Override
    public String toString() {
        return "Application{" + "id=" + id + ", type=" 
                + type + ", apps=" + apps + '}';
    }
}
